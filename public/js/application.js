$(document).ready(function () {
    var warlock = new Warlock('ws://localhost:8000');
    $('#warlockStatusSPAN').text('Connecting');
    warlock.onconnect(function () {
        $('#warlockStatusSPAN').text('Connected');
    }).onerror(function () {
        $('#warlockStatusSPAN').text('Error');
    }).onstatus(function (status) {
        $('#warlockStatusSPAN').text(status);
    });

    warlock.subscribe('say', function (packet) {
        $('#chat').append($('<div class="chatmsg">').html(packet)).scrollTop($('#chat')[0].scrollHeight);
    });

    $('#chatmsg').keypress(function (e) {
        if (e.key === 'Enter') {
            var msg = $(this).val().trim();
            if (msg) {
                warlock.trigger('say', msg, true);
                $(this).val('');
            }
        }
    });

});