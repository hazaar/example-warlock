# Hazaar Warlock Example Application

This is an exapmple application to demonstrate the simple use of the Hazaar Warlock signalling system.

## Installing with Composer

The best way to create a new Hazaar Warlock example project is to use Composer. If you don't have it already installed, then please see [their documentation](http://getcomposer.org) for installation instructions.

To create your new Hazaar Warlock project:

```
$ composer create-project hazaarlabs/example-warlock path/to/install
```

Once installed, you can test it out immediately using PHP's built-in web server:

```
$ composer serve
```

This will start the PHP cli-server on port 8080, and bind it to all network interfaces.

Note: The built-in CLI server is for development only.