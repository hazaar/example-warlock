FROM php:8.3.4-fpm-alpine3.19 AS base
ENV PHP_INI_DIR=/usr/local/etc/php
RUN apk --no-cache add musl-locales g++ make nginx \
    libpng-dev freetype-dev libjpeg-turbo-dev libpng-dev \
    libzip-dev libpq-dev openssl-dev libxml2-dev icu-dev linux-headers php83-dev;
ARG CI_ENVIRONMENT_NAME=development
ENV APPLICATION_ENV=$CI_ENVIRONMENT_NAME
RUN docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg; \
    docker-php-ext-install -j$(nproc) gd intl sockets zip xml calendar pdo_pgsql pcntl;\
    pecl install apcu; \
    docker-php-ext-enable apcu
COPY docker/nginx-${CI_ENVIRONMENT_NAME}.conf /etc/nginx/http.d/default.conf
COPY --chmod=755 docker/hazaar-*-start.sh /usr/local/bin/
COPY --chmod=755 docker/docker-php-entrypoint /usr/local/bin/docker-php-entrypoint
RUN echo 'memory_limit = 250M' >> /usr/local/etc/php/conf.d/docker-php-memlimit.ini; \
    mkdir /var/hazaar; \
    chmod 775 /var/hazaar; \
    chown www-data:www-data /var/hazaar; 

ENTRYPOINT ["docker-php-entrypoint"]
STOPSIGNAL SIGQUIT
EXPOSE 80/tcp

FROM base AS production
COPY . /opt/hazaar/
WORKDIR /opt/hazaar
CMD ["hazaar-app-start.sh"]

FROM base AS development
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer
RUN pecl install xdebug; \
    docker-php-ext-enable xdebug; \
    echo -e "\nxdebug.mode = develop, debug\nxdebug.start_with_request = 1\nxdebug.log_level = 0\n" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN apk --no-cache add bash git iputils-ping traceroute vim procps inotify-tools sudo gettext
RUN echo "alias l='ls -l'" >> ~/.bashrc
RUN mkdir /tmp/phpstan; \
    mkdir /var/hazaar/warlock