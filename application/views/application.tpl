<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="em_AU">

<head>

    <base href="{url}" target="_top" />

    <title>Testing</title>

    <link rel="stylesheet" type="text/css" href="css/application.css" />

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"
        integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g=="
        crossorigin="anonymous" referrerpolicy="no-referrer" defer></script>
    <script type="text/javascript" src="hazaar/js/warlock.js" defer></script>
    <script type="text/javascript" src="js/application.js" defer></script>

</head>

<body>
    <div>
        <a href="{url index/test}">test</a>
    </div>

    {layout}

</body>

</html>