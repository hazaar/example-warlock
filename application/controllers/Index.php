<?php

declare(strict_types=1);

namespace Application\Controllers;

use Hazaar\Application\Interfaces\Request;
use Hazaar\Controller\Action;
use Hazaar\Controller\Response;
use Hazaar\Warlock\Control;

class Index extends Action
{
    public function index(): void
    {
        $this->view('index');
    }

    public function test(): Response
    {
        $control = new Control();
        $result = $control->runDelay(2, function () {
            // @phpstan-ignore-next-line
            $this->trigger('say', 'Hello, World!');
        });

        return $this->redirect('index');
    }

    protected function init(Request $request): void
    {
        // do nothing
    }
}
