<?php

declare(strict_types=1);

namespace Application\Services;

use Hazaar\Warlock\Service;

class Chat extends Service
{
    private int $count = 0;

    public function init(): void
    {
        $this->subscribe('say', 'processTalk');
        $this->cron('* * * * *', 'sayTime');
    }

    public function processTalk(mixed $packet): void
    {
        switch ($packet) {
            case 'time':
                $this->sayTime();

                break;

            case 'yell':
                throw new \Exception('I am yelling');

            case 'die':
                $this->trigger('say', 'Goodbye!');
                $this->stop();

                break;

            default:
                if ('slave' !== substr($packet, 0, 5)) {
                    $this->log(W_INFO, $msg = APPLICATION_ENV.': '.$packet);
                    $this->trigger('say', $msg);
                }
        }
    }

    public function sayTime(): void
    {
        $this->trigger('say', 'The time is now '.date('H:i'));
    }

    public function run(): void
    {
        $this->trigger('say', $this->count++);
        $this->sleep(1);
    }
}
